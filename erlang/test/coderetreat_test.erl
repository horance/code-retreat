-module(coderetreat_test).

-compile(export_all).
-import(coderetreat, [something/0]).

-include_lib("eunit/include/eunit.hrl").

something_test() ->
    ?assert(1 =:= something()).
